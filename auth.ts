// deno-lint-ignore-file

import { encode as encodeBase64Url } from "std/encoding/base64url.ts";
import { encodeToString as encodeHex } from "std/encoding/hex.ts";
import { Message } from "std/hash/hasher.ts";
import { createHash } from "std/hash/mod.ts";
import { serve } from "std/http/server.ts";
// @deno-types="https://unpkg.com/ky/index.d.ts"
import ky from "https://unpkg.com/ky/index.js";
import jwtVerify from "https://unpkg.com/jose@3.5.0/dist/browser/jwt/verify.js";

const alphabet =
  "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~";

function sha256Hash(input: Message) {
  const hash = createHash("sha256");
  hash.update(input);
  return hash.digest();
}

export function createCodeVerifier(length = 43) {
  const characters: string[] = [];
  for (let i = 0; i < length; i++) {
    let num: number;
    do {
      ([num] = crypto.getRandomValues(new Uint8Array(1)));
    } while (num >= alphabet.length);
    characters.push(alphabet[num]);
  }
}

export function createCodeChallenge(codeVerifier: Message) {
  return encodeBase64Url(new Uint8Array(sha256Hash(codeVerifier)));
}

// https://developers.google.com/identity/protocols/oauth2/openid-connect
export async function oauth(
  {
    url = "https://accounts.google.com/o/oauth2/v2/auth",
    client_id,
    client_secret,
    code_challenge,
    code_challenge_method,
    port = 9875,
  }: {
    url?: string;
    client_id: string;
    client_secret: string;
    redirect_uri: string;
    code_challenge: string;
    code_challenge_method: "S256" | "plain";
    port?: number;
  },
) {
  // Send an authentication request to Google
  const state = encodeHex(
    (crypto.getRandomValues(new Uint8Array(16)) as Uint8Array),
  );
  const searchParams: Record<string, string> = {
    client_id,
    redirect_uri: `http://127.0.0.1:${port}/`,
    response_type: "code",
    scope:
      "https://www.googleapis.com/auth/fitness.body.read https://www.googleapis.com/auth/fitness.body.write",
    code_challenge,
    code_challenge_method,
    state,
  };
  searchParams.state = state;

  const urlSp = new URLSearchParams(searchParams);
  const urlObj = new URL(url);
  urlObj.search = urlSp.toString();

  console.log(
    `Please visit this URL in your browser to initiate authentication: ${urlObj}`,
  );

  // Receive code from Google via loopback
  const server = serve({ hostname: "0.0.0.0", port });
  console.log("Waiting for user auth.");

  let code: string;

  for await (const request of server) {
    if (request.url !== "/") {
      request.respond({ status: 404, body: "Not Found" });
    }
    if (request.method !== "GET") {
      request.respond({ status: 405, body: "Method Not Allowed" });
    }
    const urlObj = new URL(request.url);

    // Verify state
    if (urlObj.searchParams.get("state") !== state) {
      console.log("Invalid state.");
      request.respond({ status: 401 });
      continue;
    }

    code = urlObj.searchParams.get("code")!;
    request.respond({ status: 204 });
    break;
  }

  // Exchange code for access token and ID token
  const formData = new FormData();
  formData.set("code", code!);
  formData.set("client_id", client_id);
  formData.set("client_secret", client_secret);
  formData.set("redirect_uri", `http://localhost:${port}/`);
  formData.set("grant_type", "authorization_code");

  const codeResponse = await ky.post("https://oauth2.googleapis.com/token", {
    body: formData,
  }).json<CodeResponse>();

  return codeResponse;
}

interface CodeResponse {
  access_token: string;
  expires_in: number;
  id_token?: string;
  scope: string;
  token_type: string;
  refresh_token: string;
}
