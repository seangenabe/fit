import { join } from "std/path/mod.ts";
import { getPaths } from "./paths.ts";

export function getConfig(name: string) {
  const configDir = getPaths(name).config;
  if (configDir == null) return undefined;
  const configFile = join(configDir, "config.json");

  const ensureDirExists = () => Deno.mkdir(configDir, { recursive: true });

  return {
    async load<T>() {
      try {
        await ensureDirExists();
        const text = await Deno.readTextFile(configFile);
        return JSON.parse(text) as T;
      } catch (err) {
        return undefined;
      }
    },
    async save(config: unknown) {
      await ensureDirExists();
      await Deno.writeTextFile(configFile, JSON.stringify(config));
    },
  };
}
