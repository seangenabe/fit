#!/usr/bin/env sh
deno run --allow-net --allow-env --allow-read --allow-write --importmap=import_map.json ./fit.ts "$@"
