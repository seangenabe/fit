// @deno-types="https://unpkg.com/ky/index.d.ts"
import ky from "https://unpkg.com/ky/index.js";
import { parse as parseArgs } from "std/flags/mod.ts";
import { getConfig } from "./config.ts";

const dataTypes = {
  weight: {
    name: "com.google.weight",
  },
};

const args = parseArgs(Deno.args);
const [arg0, arg1, arg2, arg3] = args._;

const configController = getConfig("fit");

if (configController == null) {
  console.error("Cannot read config file.");
  Deno.exit(1);
}

if (arg0 === "client") {
  if (arg1 === "get") {
    const config = await configController.load<Config>();
    if (config) {
      console.log(`Client ID: ${config.clientId}`);
    } else {
      console.log("🤷");
    }
  } else if (arg1 === "set") {
    if (!(arg2)) {
      console.log("fit client set <client id>");
      Deno.exit(0);
    }
    const config: Config = { clientId: `${arg2}` };
    await configController.save(config);
  } else {
    console.log("fit client <get | set>");
  }
} else {
  console.log(`Available commands:
  client get
  client set
`);
}

interface Config {
  clientId: string;
}
