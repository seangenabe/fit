import { join } from "std/path/mod.ts";
const home = Deno.env.get("HOME");

function addPath(parent: string | undefined, ...paths: string[]) {
  if (parent == null) return undefined;
  return join(parent, ...paths);
}

const paths = {
  linux: (name: string) => ({
    config: addPath(
      Deno.env.get("XDG_CONFIG_HOME") ?? (home && join(home, ".config")),
      name,
    ),
  }),
  darwin: (name: string) => {
    const library = addPath(home, "Library");

    return {
      config: addPath(library, "Preferences", name),
    };
  },
  windows: (name: string) => ({
    config: addPath(Deno.env.get("APPDATA"), name, "config"),
  }),
};

export function getPaths(name: string) {
  const { os } = Deno.build;
  const id = `deno-${name}`;

  return paths[os ?? "linux"](id);
}
